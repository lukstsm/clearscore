package com.sample.clearscore.app


import com.sample.clearscore.home.HomeActivity
import com.sample.clearscore.home.HomeActivityModule

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = arrayOf(HomeActivityModule::class, TransactionsFragmentProvider::class))
    internal abstract fun bindHomeActivity(): HomeActivity

}
