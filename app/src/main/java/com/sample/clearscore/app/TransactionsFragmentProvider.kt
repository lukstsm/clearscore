package com.sample.clearscore.app

import com.sample.clearscore.creditscore.CreditScoreFragment
import com.sample.clearscore.creditscore.CreditScoreFragmentModule

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class TransactionsFragmentProvider {

    @ContributesAndroidInjector(modules = arrayOf(CreditScoreFragmentModule::class))
    internal abstract fun provideStatusFragmentFactory(): CreditScoreFragment
}
