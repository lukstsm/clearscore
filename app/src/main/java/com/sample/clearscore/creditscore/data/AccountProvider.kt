package com.sample.clearscore.creditscore.data

import io.reactivex.Observable
import javax.inject.Inject


class AccountProvider @Inject constructor(private val accountApi: AccountApi) {

    fun getCreditReport(): Observable<CreditReport> {
        return accountApi.fetchAccount()
            .map { it.creditReportInfo }
    }
}