package com.sample.clearscore.creditscore.data

import io.reactivex.Observable
import retrofit2.http.GET


interface AccountApi {

    @GET("/prod/mockcredit/values")
    fun fetchAccount(): Observable<Account>

}