package com.sample.clearscore.creditscore

import com.sample.clearscore.architecture.InitialIntention
import com.sample.clearscore.creditscore.data.CreditReport


sealed class CreditScoreIntention {
    object Initial : CreditScoreIntention(),
                     InitialIntention

    object GetLastState : CreditScoreIntention()
    object LoadCreditReport : CreditScoreIntention()
}

sealed class CreditScoreAction {
    object Initial : CreditScoreAction()
    object GetLastState : CreditScoreAction()
    object FetchCreditReport : CreditScoreAction()
}

sealed class CreditScoreResult {
    object LastState : CreditScoreResult()
    object CreditReportLoading : CreditScoreResult()
    data class CreditReportSuccess(val creditReport: CreditReport) : CreditScoreResult()
    object CreditReportFailure : CreditScoreResult()
}

data class CreditScoreState(
        var creditReport: CreditReport? = null,
        val showLoading: Boolean = false,
        val showError: Boolean = false
)