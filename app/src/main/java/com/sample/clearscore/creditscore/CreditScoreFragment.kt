package com.sample.clearscore.creditscore

import android.content.Context
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sample.clearscore.R
import dagger.android.support.AndroidSupportInjection
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.sample_fragment.error
import kotlinx.android.synthetic.main.sample_fragment.progress
import kotlinx.android.synthetic.main.sample_fragment.score_circle_progress
import kotlinx.android.synthetic.main.sample_fragment.score_container
import kotlinx.android.synthetic.main.sample_fragment.score_index
import javax.inject.Inject
import android.util.TypedValue




class CreditScoreFragment : Fragment() {

    companion object {
        fun newInstance(): CreditScoreFragment {
            return CreditScoreFragment()
        }
    }

    @Inject
    lateinit var presenter: CreditScorePresenter

    private var intentionsSubject = PublishSubject.create<CreditScoreIntention>()

    private lateinit var disposables: CompositeDisposable

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.sample_fragment, container, false)
    }

    override fun onStart() {
        super.onStart()
        disposables = CompositeDisposable(
            presenter.states()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::render),
            presenter.processIntentions(intentions())
        )
    }

    override fun onStop() {
        super.onStop()
        disposables.dispose()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.destroy()
    }


    private fun intentions() = Observable.merge(
        Observable.just(CreditScoreIntention.Initial),
        Observable.just(CreditScoreIntention.LoadCreditReport),
        intentionsSubject
    )

    private fun render(state: CreditScoreState) {
        progress.visibility = if (state.showLoading) View.VISIBLE else View.GONE
        error.visibility = if (state.showError) View.VISIBLE else View.GONE
        score_container.visibility = if (state.creditReport != null) View.VISIBLE else View.GONE

        state.creditReport?.let {
            val colour = getScoreColour(it.score, it.maxScoreValue)
            score_circle_progress.max = it.maxScoreValue
            score_circle_progress.progress = it.score
            score_circle_progress.progressDrawable.setColorFilter(colour, PorterDuff.Mode.SRC_IN)
            score_index.text = getScoreIndexSpannable(it.score, it.maxScoreValue, colour)
        }
    }

    //Below 2 methods are examples of logic that could be moved to UI model
    private fun getScoreColour(score: Int, maxScore: Int): Int {
        val percentage = score.toFloat() / maxScore
        return if (percentage > 0.7) {
            ContextCompat.getColor(activity, R.color.credit_score_good)
        } else if (percentage > 0.5) {
            ContextCompat.getColor(activity, R.color.credit_score_okay)
        } else {
            ContextCompat.getColor(activity, R.color.credit_score_bad)
        }
    }

    private fun getScoreIndexSpannable(score: Int, maxScore: Int, colour: Int): Spannable {
        val message = getString(R.string.credit_score_message_format).format(score, maxScore)
        val spannableString = SpannableStringBuilder(message)

        val spanStart = spannableString.indexOf(score.toString(), 0, false)
        val spanEnd = spanStart + score.toString().length

        val typedValue = TypedValue()
        resources.getValue(R.dimen.credit_score_rating_size_multiplier, typedValue, true)

        spannableString.setSpan(RelativeSizeSpan(typedValue.float), spanStart, spanEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannableString.setSpan(ForegroundColorSpan(colour), spanStart, spanEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

        return spannableString
    }

}