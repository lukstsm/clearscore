package com.sample.clearscore.creditscore

import com.sample.clearscore.creditscore.data.AccountApi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


@Module
class CreditScoreFragmentModule {

    //In production project this would be broken down and utilise some build flags to
    //switch between prod/uat/mock, set logging level, etc.

    @Provides
    fun provideAccountApi(): AccountApi {

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        val client = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl("https://5lfoiyb0b3.execute-api.us-west-2.amazonaws.com")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

        return retrofit.create(AccountApi::class.java)
    }

}
