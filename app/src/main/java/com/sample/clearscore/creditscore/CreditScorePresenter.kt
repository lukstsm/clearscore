package com.sample.clearscore.creditscore

import com.sample.clearscore.architecture.MVIBasePresenter
import javax.inject.Inject


class CreditScorePresenter @Inject constructor(
        interactor: CreditScoreInteractor
) : MVIBasePresenter<CreditScoreIntention, CreditScoreAction, CreditScoreResult, CreditScoreState>(interactor) {

    override val defaultState: CreditScoreState
        get() = CreditScoreState()

    override val lastStateIntention: CreditScoreIntention
        get() = CreditScoreIntention.GetLastState

    override fun intentionToActionMapper(): (CreditScoreIntention) -> CreditScoreAction = {
        when (it) {
            is CreditScoreIntention.Initial -> CreditScoreAction.Initial
            is CreditScoreIntention.GetLastState -> CreditScoreAction.GetLastState
            is CreditScoreIntention.LoadCreditReport -> CreditScoreAction.FetchCreditReport
        }
    }

    override fun stateReducer(): (CreditScoreState, CreditScoreResult) -> CreditScoreState = { prevState, result ->
        when (result) {
            is CreditScoreResult.LastState -> prevState.copy()
            is CreditScoreResult.CreditReportLoading -> prevState.copy(
                showLoading = true
            )
            is CreditScoreResult.CreditReportSuccess -> prevState.copy(
                creditReport = result.creditReport,
                showLoading = false
            )
            is CreditScoreResult.CreditReportFailure -> prevState.copy(
                showLoading = false,
                showError = true
            )
        }
    }
}