package com.sample.clearscore.creditscore.data

data class Account(val creditReportInfo: CreditReport)

data class CreditReport(val score: Int, val maxScoreValue: Int, val minScoreValue: Int)
