package com.sample.clearscore.creditscore

import com.sample.clearscore.architecture.MVIInteractor
import com.sample.clearscore.creditscore.data.AccountProvider
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class CreditScoreInteractor @Inject constructor(
        private val accountProvider: AccountProvider
) : MVIInteractor<CreditScoreAction, CreditScoreResult> {

    override fun actionProcessor(): ObservableTransformer<in CreditScoreAction, out CreditScoreResult> {
        return ObservableTransformer {
            it.observeOn(Schedulers.io())
                .flatMap { action ->
                    when (action) {
                        is CreditScoreAction.Initial -> getLastState()
                        is CreditScoreAction.GetLastState -> getLastState()
                        is CreditScoreAction.FetchCreditReport -> fetchTransactions()
                    }
                }
        }
    }

    private fun getLastState(): Observable<CreditScoreResult> = Observable.just(CreditScoreResult.LastState)

    //In prod project there would be a layer mapping between network and domain (and possibly ui) models
    //to ensure view state is not coupled to api/db interface
    internal fun fetchTransactions() = accountProvider.getCreditReport()
        .map { CreditScoreResult.CreditReportSuccess(it) as CreditScoreResult }
        .onErrorReturnItem(CreditScoreResult.CreditReportFailure)
        .startWith(CreditScoreResult.CreditReportLoading)

}