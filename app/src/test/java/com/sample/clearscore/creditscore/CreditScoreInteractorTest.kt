package com.sample.clearscore.creditscore

import com.sample.clearscore.creditscore.data.AccountProvider
import com.sample.clearscore.creditscore.data.CreditReport
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class CreditScoreInteractorTest {

    private val creditReport = CreditReport(
        50,
        0,
        100
    )

    @Mock
    private lateinit var accountProvider: AccountProvider

    private lateinit var interactor: CreditScoreInteractor

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        interactor = CreditScoreInteractor(accountProvider)
    }

    @Test
    fun interactorReturnsReportWhenRequestSuccessful() {
        `when`(accountProvider.getCreditReport())
            .thenReturn(Observable.just(creditReport))

        interactor.fetchTransactions()
            .test()
            .assertValueAt(0, CreditScoreResult.CreditReportLoading)
            .assertValueAt(1, CreditScoreResult.CreditReportSuccess(creditReport))
    }

    @Test
    fun interactorReturnsErrorWhenRequestFails() {
        `when`(accountProvider.getCreditReport())
            .thenReturn(Observable.error(Exception("The end is nigh!")))

        interactor.fetchTransactions()
            .test()
            .assertValueAt(0, CreditScoreResult.CreditReportLoading)
            .assertValueAt(1, CreditScoreResult.CreditReportFailure)
    }


}